/*
    AWFFull - A Webalizer Fork, Full o' features

    linklist.h
        definitions for dealing with linked lists in awffull
    
    Copyright (C) 1997-2001  Bradford L. Barrett (brad@mrunix.net)
    Copyright (C) 2006, 2008 by Stephen McInerney (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AWFFULL_LINKLIST_H
#define AWFFULL_LINKLIST_H

#include <limits.h>

#define LARGE 32767                             /* A really big number for dealing with B-M-H */

struct nlist {
    char string[80];                            /* list struct for HIDE & GROUP items   */
    char name[80];
    size_t length;
    int last[UCHAR_MAX + 1];
    int HorspoolSkip2;
    bool wildcard_start;
    bool wildcard_end;
    unsigned long matched;                      /* how often have this entry matched? */
    struct nlist *next;
};
typedef struct nlist *LISTPTR;

extern LISTPTR group_sites;                     /* "group" lists            */
extern LISTPTR group_urls;
extern LISTPTR group_refs;
extern LISTPTR group_agents;
extern LISTPTR group_users;
extern LISTPTR hidden_sites;                    /* "hidden" lists           */
extern LISTPTR hidden_urls;
extern LISTPTR hidden_refs;
extern LISTPTR hidden_agents;
extern LISTPTR hidden_users;
extern LISTPTR ignored_sites;                   /* "Ignored" lists          */
extern LISTPTR ignored_urls;
extern LISTPTR ignored_refs;
extern LISTPTR ignored_agents;
extern LISTPTR ignored_users;
extern LISTPTR include_sites;                   /* "Include" lists          */
extern LISTPTR include_urls;
extern LISTPTR include_refs;
extern LISTPTR include_agents;
extern LISTPTR include_users;
extern LISTPTR index_alias;                     /* index. aliases            */
extern LISTPTR html_pre;                        /* before anything else :)   */
extern LISTPTR html_head;                       /* top HTML code             */
extern LISTPTR html_body;                       /* body HTML code            */
extern LISTPTR html_post;                       /* middle HTML code          */
extern LISTPTR html_tail;                       /* tail HTML code            */
extern LISTPTR html_end;                        /* after everything else     */
extern LISTPTR page_type;                       /* page view types           */
extern LISTPTR not_page_type;                   /* NOT page view types       */
extern LISTPTR search_list;                     /* Search engine list        */
extern LISTPTR assign_country;                  /* Assign Address to Country */

extern LISTPTR seg_countries;                   /* Segment by Countries     */
extern LISTPTR seg_referers;                    /* Segment by Referers     */

extern char *isinlist(LISTPTR, char *);         /* scan list for str   */

extern int add_list_member(char *, LISTPTR *, bool);    /* add group list item */
extern void show_matched(LISTPTR, char *);

#endif          /* AWFFULL_LINKLIST_H */
