/*
    AWFFull - A Webalizer Fork, Full o' features

    xmalloc.h
        memory handling wrapper

    Copyright (C) 2006, 2008 by Stephen McInerney
        (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AWFFULL_XMALLOC_H
#define AWFFULL_XMALLOC_H 1


/************************************************************************
 *                              MACROS                                  *
 ************************************************************************/
#define XMALLOC(type, count) ((type *) xmalloc ((count) * sizeof(type)))
#define XFREE(stuff) free (stuff); stuff = NULL;


/************************************************************************
 *                              GLOBALS                                 *
 ************************************************************************/
extern void *xmalloc(size_t num);


#endif          /* AWFFULL_XMALLOC_H */

/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************
 ************************************************************************/
