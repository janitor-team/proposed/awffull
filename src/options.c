/*
    AWFFull - A Webalizer Fork, Full o' features
    
    options.c
        Process command line options
        set default flag/option values

    Copyright (C) 2004-2008 by Stephen McInerney (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "awffull.h"                            /* main header              */

/************************************************************************
 *                              GLOBALS                                 *
 ************************************************************************/

/************************************************************************
 *                              FUNCTIONS                               *
 ************************************************************************/
void process_options(int, char *[]);            /* Process command line options */
static void display_version(void);              /* Display the Version Information */
static void display_usage(int);                 /* Display the program Command Line Options, and exit */
static void display_gnu_info(int);              /* Display the GNU Disclaimer, and exit */
void set_defaults(void);                        /* Use the default-global structures to set the default settings */
void display_options(void);                     /* Dump to STDOUT all the user set options */

/************************************************************************
 * display_version                                                      *
 *                                                                      *
 * Display the Version Information                                      *
 ************************************************************************/
static void
display_version(void)
{
    printf("%s\n", PACKAGE_STRING);
    printf(_("\
Copyright 1997-2001 by Bradford L. Barrett\n\
Copyright 2004-2008 by Steve McInerney\n\
Other copyrights, see the source files\n"));

}


/************************************************************************
 * display_usage                                                        *
 *                                                                      *
 * Display the Command Line Use and Exit with the passed exit_code      *
 ************************************************************************/
static void
display_usage(int exit_code)
{
    display_version();
    printf("%s: %s %s\n%s", _("Usage"), PACKAGE, _("[options] [log file]"), h_msg);
    exit(exit_code);
}


/************************************************************************
 * display_gnu_info                                                     *
 *                                                                      *
 * Display the GNU standard disclaimer                                  *
 ************************************************************************/
static void
display_gnu_info(int exit_code)
{
    display_version();
    printf(_("\
Licence GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n"));
    exit(exit_code);
}


/************************************************************************
 * process_options                                                      *
 *                                                                      *
 * Process the individual commandline arguments                         *
 ************************************************************************/
void
process_options(int argc, char *argv[])
{
    int opt;
    int option_index = 0;

    const char *short_options = "a:A:c:C:de:E:fF:g:GhHiI:lLm:M:n:o:pP:qQr:R:s:S:t:Tu:U:vVx:XYZ:?";
    const struct option long_options[] = {
        {"config", 1, NULL, 'c'},
        {"debug", 0, NULL, 'd'},
        {"disable_file_checks", 0, NULL, 0},
        {"fold", 0, NULL, 'f'},
        {"help", 0, NULL, 'h'},
        {"ignore_history", 0, NULL, 'i'},
        {"logtype", 1, NULL, 'F'},
        {"match_counts", 0, NULL, 0},
        {"output", 1, NULL, 'o'},
        {"preserve_state", 0, NULL, 'p'},
        {"quiet", 0, NULL, 'q'},
        {"timing", 0, NULL, 'T'},
        {"top_agents", 1, NULL, 'A'},
        {"top_countries", 1, NULL, 'C'},
        {"top_entry", 1, NULL, 'e'},
        {"top_exit", 1, NULL, 'E'},
        {"top_refers", 1, NULL, 'R'},
        {"top_sites", 1, NULL, 'S'},
        {"top_urls", 1, NULL, 'U'},
        {"use_geoip", 0, NULL, 0},
        {"verbose", 0, NULL, 'v'},
        {"version", 0, NULL, 'V'},
        {"vquiet", 0, NULL, 'Q'},
        {"seg_country", 1, NULL, 0},
        {"seg_referer", 1, NULL, 0},
        {"seg_search", 1, NULL, 0},
        {NULL, 0, NULL, 0}
    };

    do {
        opt = getopt_long(argc, argv, short_options, long_options, &option_index);
        switch (opt) {
        case 0:
            if (strncmp("match_counts", long_options[option_index].name, strlen("match_counts")) == 0) {
                g_settings.flags.display_match_counts = true;
            } else if (strncmp("use_geoip", long_options[option_index].name, strlen("use_geoip")) == 0) {
                g_settings.flags.use_geoip = true;
            } else if (strncmp("seg_country", long_options[option_index].name, strlen("seg_country")) == 0) {
                /* TODO: Move the vprint to a listdump at the end of all options loaded */
                add_list_member(strtoupper(optarg), &seg_countries, IGNORESPACE);
                g_settings.flags.segcountry = true;
                VPRINT(VERBOSE1, "Segmenting by Country: %s\n", optarg);
                g_settings.flags.segmenting = true;
            } else if (strncmp("seg_referer", long_options[option_index].name, strlen("seg_referer")) == 0) {
                add_list_member(optarg, &seg_referers, IGNORESPACE);
                g_settings.flags.segreferer = true;
                VPRINT(VERBOSE1, "Segmenting by Referer: %s\n", optarg);
                g_settings.flags.segmenting = true;
            } else if (strncmp("disable_file_checks", long_options[option_index].name, strlen("disable_file_checks")) == 0) {
                VPRINT(VERBOSE1, "Disabling Checks for Existing CSS and Monthly Reports\n");
                g_settings.flags.disable_report_file_checks = true;
            }
            break;
        case 'a':
            add_list_member(optarg, &hidden_agents, USESPACE);
            break;                              /* Hide agents   */
        case 'A':
            g_settings.top.agents = atoi(optarg);
            break;                              /* Top agents          */
        case 'c':
            get_config(optarg);
            break;                              /* Config file         */
        case 'C':
            g_settings.top.countries = atoi(optarg);
            break;                              /* Top countries       */
        case 'd':
            ERRVPRINT(VERBOSE1, "%s\n", _("Use of \"Debug\" has been deprecated."));
            break;                              /* Debug               */
        case 'e':
            g_settings.top.entry = atoi(optarg);
            break;                              /* Top entry pages     */
        case 'E':
            g_settings.top.exit = atoi(optarg);
            break;                              /* Top exit pages      */
        case 'f':
            g_settings.flags.fold_seq_err = true;
            break;                              /* Fold sequence errs  */
        case 'F':
            g_settings.flags.force_log_type = true;
            if (strncmp(optarg, "auto", 4) == 0) {
                g_settings.settings.log_type = LOG_AUTO;
                g_settings.flags.force_log_type = false;
            } else if (strncmp(optarg, "clf", 3) == 0) {
                g_settings.settings.log_type = LOG_CLF;
            } else if (strncmp(optarg, "ftp", 3) == 0) {
                g_settings.settings.log_type = LOG_FTP;
            } else if (strncmp(optarg, "squid", 5) == 0) {
                g_settings.settings.log_type = LOG_SQUID;
            } else if (strncmp(optarg, "combined", 8) == 0) {
                g_settings.settings.log_type = LOG_COMBINED;
            } else if (strncmp(optarg, "domino", 8) == 0) {
                g_settings.settings.log_type = LOG_DOMINO;
            } else {
                ERRVPRINT(VERBOSE0, "%s %s\n", _("Unknown Log Type:"), optarg);
                display_usage(0);
                exit(1);
            }
            break;                              /* define log type     */
        case 'g':
            g_settings.settings.group_domains = atoi(optarg);
            break;                              /* GroupDomains (0=no) */
        case 'G':
            g_settings.graphs.hourly = false;
            break;                              /* no hourly graph     */
        case 'h':
            display_usage(0);
            break;                              /* help                */
        case 'H':
            g_settings.stats.hourly = false;
            break;                              /* no hourly stats     */
        case 'i':
            g_settings.flags.ignore_history = true;
            break;                              /* Ignore history      */
        case 'I':
            add_list_member(optarg, &index_alias, USESPACE);
            break;                              /* Index alias     */
        case 'l':
            g_settings.graphs.lines = false;
            break;                              /* Graph Lines         */
        case 'L':
            g_settings.graphs.legend = false;
            break;                              /* Graph Legends       */
        case 'm':
            g_settings.settings.visit_timeout = atoi(optarg);
            break;                              /* Visit Timeout       */
        case 'M':
            g_settings.settings.mangle_agent = atoi(optarg);
            break;                              /* mangle user agents  */
        case 'n':
            g_settings.settings.hostname = optarg;
            break;                              /* Hostname            */
        case 'o':
            g_settings.settings.out_dir = optarg;
            break;                              /* Output directory    */
        case 'p':
            g_settings.flags.incremental = true;
            break;                              /* Incremental run     */
        case 'P':
            add_list_member(optarg, &page_type, USESPACE);
            break;                              /* page view types   */
        case 'q':
            ERRVPRINT(VERBOSE1, "%s\n", _("Use of \"Quiet (-q)\" has been deprecated."));
            break;                              /* Quiet (verbose=1)   */
        case 'Q':
            ERRVPRINT(VERBOSE1, "%s\n", _("Use of \"Really Quiet (-Q)\" has been deprecated."));
            break;                              /* Really Quiet        */
        case 'r':
            add_list_member(optarg, &hidden_refs, USESPACE);
            break;                              /* Hide referrer */
        case 'R':
            g_settings.top.refs = atoi(optarg);
            break;                              /* Top referrers       */
        case 's':
            add_list_member(optarg, &hidden_sites, USESPACE);
            break;                              /* Hide site     */
        case 'S':
            g_settings.top.sites = atoi(optarg);
            break;                              /* Top sites           */
        case 't':
            g_settings.settings.title_message = optarg;
            break;                              /* Report title        */
        case 'T':
            g_settings.flags.time_me = true;
            break;                              /* TimeMe              */
        case 'u':
            add_list_member(optarg, &hidden_urls, USESPACE);
            break;                              /* hide URL      */
        case 'U':
            g_settings.top.urls = atoi(optarg);
            break;                              /* Top urls            */
        case 'x':
            g_settings.settings.html_ext = optarg;
            break;                              /* HTML file extension */
        case 'X':
            g_settings.flags.hide_sites = true;
            break;                              /* Hide ind. sites     */
        case 'Y':
            g_settings.graphs.country = false;
            break;                              /* Supress ctry graph  */
        case 'Z':
            g_settings.settings.index_months = atoi(optarg);
            break;                              /* Number of Months to display on the main Index  */
        case 'v':
            g_settings.settings.verbosity++;
            if (g_settings.settings.verbosity > VERBOSE_MAX) {
                g_settings.settings.verbosity = VERBOSE_MAX;
                ERRVPRINT(VERBOSE1, "%s\n", _("Warning: Too many 'v' options. Setting to Max Verboseness"));
            }
            break;
        case 'V':
            display_gnu_info(0);
            break;
        case '?':
            display_usage(0);
            break;
        case -1:
            break;
        default:
            exit(1);                            /* FIXME! */
        }
    } while (opt != -1);
}


/************************************************************************
 * set_defaults                                                         *
 *                                                                      *
 * Set all default Global Values                                        *
 ************************************************************************/
void
set_defaults(void)
{
    /* Zero both Global Structures as a first check */
    memset(&g_counters, 0, sizeof(g_counters));
    memset(&g_settings, 0, sizeof(g_settings));

    /* Set default settings. Note that many of these are redundant following the memset. */
    g_settings.graphs.hourly = true;
    g_settings.graphs.daily = true;
    g_settings.graphs.lines = true;
    g_settings.graphs.legend = true;
    g_settings.graphs.country = true;
    g_settings.graphs.url_by_hits = false;
    g_settings.graphs.url_by_vol = false;
    g_settings.graphs.sites_by_pages = false;
    g_settings.graphs.sites_by_vol = false;
    g_settings.graphs.agents = false;
    g_settings.graphs.exit_pages = 0;
    g_settings.graphs.entry_pages = 0;
    g_settings.graphs.use_kanji = false;
    g_settings.graphs.index_x = GRAPH_INDEX_X;
    g_settings.graphs.index_y = GRAPH_INDEX_Y;
    g_settings.graphs.daily_x = GRAPH_DAILY_X;
    g_settings.graphs.daily_y = GRAPH_DAILY_Y;
    g_settings.graphs.hourly_x = GRAPH_HOURLY_X;
    g_settings.graphs.hourly_y = GRAPH_HOURLY_Y;
    g_settings.graphs.pie_x = GRAPH_PIE_X;
    g_settings.graphs.pie_y = GRAPH_PIE_Y;

    g_settings.stats.hourly = true;
    g_settings.stats.daily = true;

    g_settings.flags.time_me = false;
    g_settings.flags.local_time = true;
    g_settings.flags.ignore_history = false;
    g_settings.flags.shade_groups = true;
    g_settings.flags.highlight_groups = true;
    g_settings.flags.incremental = false;
    g_settings.flags.incremental_duplicate_check = false;
    g_settings.flags.use_https = false;
    g_settings.flags.fold_seq_err = false;
    g_settings.flags.hide_sites = false;
    g_settings.flags.track_206_reqs = false;
    g_settings.flags.display_match_counts = false;
    g_settings.flags.display_yearly_subtotals = false;
    g_settings.flags.force_log_type = false;
    g_settings.flags.use_geoip = false;
    g_settings.flags.have_geoip = false;
    g_settings.flags.cumulative_percentages = true;
    g_settings.flags.segmenting = false;
    g_settings.flags.segcountry = false;
    g_settings.flags.segreferer = false;
    g_settings.flags.disable_report_file_checks = false;
    g_settings.flags.ignore_index_alias = false;
    g_settings.flags.is_first_run = true;

    g_settings.all.sites = false;
    g_settings.all.urls = false;
    g_settings.all.refs = false;
    g_settings.all.agents = false;
    g_settings.all.search = false;
    g_settings.all.users = false;
    g_settings.all.errors = false;
    g_settings.all.entry = false;
    g_settings.all.exit = false;

    g_settings.dump.sites = false;
    g_settings.dump.urls = false;
    g_settings.dump.refs = false;
    g_settings.dump.agents = false;
    g_settings.dump.users = false;
    g_settings.dump.search = false;
    g_settings.dump.countries = false;
    g_settings.dump.entry_pages = false;
    g_settings.dump.exit_pages = false;
    g_settings.dump.header = false;
    g_settings.dump.dump_path = NULL;
    g_settings.dump.dump_ext = "tab";

    g_settings.settings.index_months = MAXHISTLEN;
    g_settings.settings.mangle_agent = 0;
    g_settings.settings.visit_timeout = 1800;
    g_settings.settings.log_type = LOG_AUTO;
    g_settings.settings.group_domains = 0;
    g_settings.settings.verbosity = VERBOSE0;
    g_settings.settings.hostname = NULL;
    // The default title to display on the HTML Reports
    g_settings.settings.title_message = _("Usage Statistics for");
    g_settings.settings.state_filename = FILENAME_CURRENT;
    g_settings.settings.history_filename = FILENAME_HISTORY;
    g_settings.settings.css_filename = FILENAME_CSS;

    g_settings.settings.config_filename = XMALLOC(char, strlen(ETCDIR) + strlen(FILENAME_CONFIG) + 2);

    snprintf(g_settings.settings.config_filename, strlen(ETCDIR) + strlen(FILENAME_CONFIG) + 2, "%s/%s", ETCDIR, FILENAME_CONFIG);

    g_settings.settings.html_ext = "html";
    g_settings.settings.out_dir = NULL;
    g_settings.settings.flags_location = NULL;
    g_settings.settings.geoip_database = GEOIP_DATABASE;
    g_settings.settings.history_index = 0;

    g_settings.top.sites = 30;
    g_settings.top.sites_by_vol = 10;
    g_settings.top.urls = 30;
    g_settings.top.urls_by_vol = 10;
    g_settings.top.entry = 10;
    g_settings.top.exit = 10;
    g_settings.top.refs = 30;
    g_settings.top.agents = 15;
    g_settings.top.countries = 30;
    g_settings.top.search = 20;
    g_settings.top.users = 20;
    g_settings.top.error = 0;

}


/************************************************************************
 * display_options                                                      *
 *                                                                      *
 * Dump to STDOUT all the user set options                              *
 ************************************************************************/
void
display_options(void)
{
    if (g_settings.settings.verbosity >= VERBOSE2) {
        VPRINT(0, "%s:\n", "Configurable Options & Flags");

        VPRINT(0, "g_settings.graphs.hourly: %d\n", g_settings.graphs.hourly);
        VPRINT(0, "g_settings.graphs.daily: %d\n", g_settings.graphs.daily);
        VPRINT(0, "g_settings.graphs.lines: %d\n", g_settings.graphs.lines);
        VPRINT(0, "g_settings.graphs.legend: %d\n", g_settings.graphs.legend);
        VPRINT(0, "g_settings.graphs.country: %d\n", g_settings.graphs.country);
        VPRINT(0, "g_settings.graphs.url_by_hits: %d\n", g_settings.graphs.url_by_hits);
        VPRINT(0, "g_settings.graphs.url_by_vol: %d\n", g_settings.graphs.url_by_vol);
        VPRINT(0, "g_settings.graphs.sites_by_pages: %d\n", g_settings.graphs.sites_by_pages);
        VPRINT(0, "g_settings.graphs.sites_by_vol: %d\n", g_settings.graphs.sites_by_vol);
        VPRINT(0, "g_settings.graphs.agents: %d\n", g_settings.graphs.agents);
        VPRINT(0, "g_settings.graphs.exit_pages: %d\n", g_settings.graphs.exit_pages);
        VPRINT(0, "g_settings.graphs.entry_pages: %d\n", g_settings.graphs.entry_pages);
        VPRINT(0, "g_settings.graphs.use_kanji: %d\n", g_settings.graphs.use_kanji);
        VPRINT(0, "g_settings.graphs.index_x: %d\n", g_settings.graphs.index_x);
        VPRINT(0, "g_settings.graphs.index_y: %d\n", g_settings.graphs.index_y);
        VPRINT(0, "g_settings.graphs.daily_x: %d\n", g_settings.graphs.daily_x);
        VPRINT(0, "g_settings.graphs.daily_y: %d\n", g_settings.graphs.daily_y);
        VPRINT(0, "g_settings.graphs.hourly_x: %d\n", g_settings.graphs.hourly_x);
        VPRINT(0, "g_settings.graphs.hourly_y: %d\n", g_settings.graphs.hourly_y);
        VPRINT(0, "g_settings.graphs.pie_x: %d\n", g_settings.graphs.pie_x);
        VPRINT(0, "g_settings.graphs.pie_y: %d\n", g_settings.graphs.pie_y);

        VPRINT(0, "g_settings.stats.hourly: %u\n", g_settings.stats.hourly);
        VPRINT(0, "g_settings.stats.daily: %u\n", g_settings.stats.daily);

        VPRINT(0, "g_settings.flags.time_me: %u\n", g_settings.flags.time_me);
        VPRINT(0, "g_settings.flags.local_time: %u\n", g_settings.flags.local_time);
        VPRINT(0, "g_settings.flags.ignore_history: %u\n", g_settings.flags.ignore_history);
        VPRINT(0, "g_settings.flags.shade_groups: %u\n", g_settings.flags.shade_groups);
        VPRINT(0, "g_settings.flags.highlight_groups: %u\n", g_settings.flags.highlight_groups);
        VPRINT(0, "g_settings.flags.incremental: %u\n", g_settings.flags.incremental);
        VPRINT(0, "g_settings.flags.use_https: %u\n", g_settings.flags.use_https);
        VPRINT(0, "g_settings.flags.fold_seq_err: %u\n", g_settings.flags.fold_seq_err);
        VPRINT(0, "g_settings.flags.hide_sites: %u\n", g_settings.flags.hide_sites);
        VPRINT(0, "g_settings.flags.track_206_reqs: %u\n", g_settings.flags.track_206_reqs);
        VPRINT(0, "g_settings.flags.display_match_counts: %u\n", g_settings.flags.display_match_counts);
        VPRINT(0, "g_settings.flags.display_yearly_subtotals: %u\n", g_settings.flags.display_yearly_subtotals);
        VPRINT(0, "g_settings.flags.force_log_type: %u\n", g_settings.flags.force_log_type);
        VPRINT(0, "g_settings.flags.use_geoip: %u\n", g_settings.flags.use_geoip);
        VPRINT(0, "g_settings.flags.have_geoip: %u\n", g_settings.flags.have_geoip);
        VPRINT(0, "g_settings.flags.cumulative_percentages: %u\n", g_settings.flags.cumulative_percentages);
        VPRINT(0, "g_settings.flags.segmenting: %u\n", g_settings.flags.segmenting);
        VPRINT(0, "g_settings.flags.segcountry: %u\n", g_settings.flags.segcountry);
        VPRINT(0, "g_settings.flags.segreferer: %u\n", g_settings.flags.segreferer);
        VPRINT(0, "g_settings.flags.disable_stat_checks: %u\n", g_settings.flags.disable_report_file_checks);
        VPRINT(0, "g_settings.flags.ignore_index_alias: %u\n", g_settings.flags.ignore_index_alias);
        VPRINT(0, "g_settings.flags.is_first_run: %u\n", g_settings.flags.is_first_run);

        VPRINT(0, "g_settings.all.sites: %u\n", g_settings.all.sites);
        VPRINT(0, "g_settings.all.urls: %u\n", g_settings.all.urls);
        VPRINT(0, "g_settings.all.refs: %u\n", g_settings.all.refs);
        VPRINT(0, "g_settings.all.agents: %u\n", g_settings.all.agents);
        VPRINT(0, "g_settings.all.search: %u\n", g_settings.all.search);
        VPRINT(0, "g_settings.all.users: %u\n", g_settings.all.users);
        VPRINT(0, "g_settings.all.errors: %u\n", g_settings.all.errors);
        VPRINT(0, "g_settings.all.entry: %u\n", g_settings.all.entry);
        VPRINT(0, "g_settings.all.exit: %u\n", g_settings.all.exit);

        VPRINT(0, "g_settings.dump.sites: %u\n", g_settings.dump.sites);
        VPRINT(0, "g_settings.dump.urls: %u\n", g_settings.dump.urls);
        VPRINT(0, "g_settings.dump.refs: %u\n", g_settings.dump.refs);
        VPRINT(0, "g_settings.dump.agents: %u\n", g_settings.dump.agents);
        VPRINT(0, "g_settings.dump.users: %u\n", g_settings.dump.users);
        VPRINT(0, "g_settings.dump.search: %u\n", g_settings.dump.search);
        VPRINT(0, "g_settings.dump.countries: %u\n", g_settings.dump.countries);
        VPRINT(0, "g_settings.dump.entry_pages: %u\n", g_settings.dump.entry_pages);
        VPRINT(0, "g_settings.dump.exit_pages: %u\n", g_settings.dump.exit_pages);
        VPRINT(0, "g_settings.dump.header: %u\n", g_settings.dump.header);
        VPRINT(0, "g_settings.dump.dump_path: %s\n", g_settings.dump.dump_path);
        VPRINT(0, "g_settings.dump.dump_ext: %s\n", g_settings.dump.dump_ext);

        VPRINT(0, "g_settings.settings.mangle_agent: %u\n", g_settings.settings.mangle_agent);
        VPRINT(0, "g_settings.settings.visit_timeout: %u\n", g_settings.settings.visit_timeout);
        VPRINT(0, "g_settings.settings.log_type: %u\n", g_settings.settings.log_type);
        VPRINT(0, "g_settings.settings.group_domains: %u\n", g_settings.settings.group_domains);
        VPRINT(0, "g_settings.settings.hostname: %s\n", g_settings.settings.hostname);
        VPRINT(0, "g_settings.settings.title_message: %s\n", g_settings.settings.title_message);
        VPRINT(0, "g_settings.settings.state_filename: %s\n", g_settings.settings.state_filename);
        VPRINT(0, "g_settings.settings.history_filename: %s\n", g_settings.settings.history_filename);
        VPRINT(0, "g_settings.settings.html_ext: %s\n", g_settings.settings.html_ext);
        VPRINT(0, "g_settings.settings.css_filename: %s\n", g_settings.settings.css_filename);
        VPRINT(0, "g_settings.settings.config_filename: %s\n", g_settings.settings.config_filename);
        VPRINT(0, "g_settings.settings.out_dir: %s\n", g_settings.settings.out_dir);
        VPRINT(0, "g_settings.settings.index_months: %u\n", g_settings.settings.index_months);
        VPRINT(0, "g_settings.settings.flags_location: %s\n", g_settings.settings.flags_location);
        VPRINT(0, "g_settings.settings.geoip_database: %s\n", g_settings.settings.geoip_database);
        VPRINT(0, "g_settings.settings.verbosity: %u\n", g_settings.settings.verbosity);

        VPRINT(0, "g_settings.top.sites: %u\n", g_settings.top.sites);
        VPRINT(0, "g_settings.top.sites_by_vol: %u\n", g_settings.top.sites_by_vol);
        VPRINT(0, "g_settings.top.urls: %u\n", g_settings.top.urls);
        VPRINT(0, "g_settings.top.urls_by_vol: %u\n", g_settings.top.urls_by_vol);
        VPRINT(0, "g_settings.top.entry: %u\n", g_settings.top.entry);
        VPRINT(0, "g_settings.top.exit: %u\n", g_settings.top.exit);
        VPRINT(0, "g_settings.top.refs: %u\n", g_settings.top.refs);
        VPRINT(0, "g_settings.top.agents: %u\n", g_settings.top.agents);
        VPRINT(0, "g_settings.top.countries: %u\n", g_settings.top.countries);
        VPRINT(0, "g_settings.top.search: %u\n", g_settings.top.search);
        VPRINT(0, "g_settings.top.users: %u\n", g_settings.top.users);
        VPRINT(0, "g_settings.top.error: %u\n", g_settings.top.error);
    }
}

/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************
 ************************************************************************/
