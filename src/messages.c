/*
    AWFFull - A Webalizer Fork, Full o' features
    
    messages.c
        Setup for common messages to the cmd line

    Copyright (C) 2004-2008 by Stephen McInerney (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

/***************************************************************************
 ***************************************************************************
 * messages.c
 *
 * More complex messages vs in line translations et al.
 * Formally 'awffull_lang.c'
 ***************************************************************************
 ***************************************************************************/

#include "awffull.h"                            /* main header              */


const char *h_msg;
const char *s_month[12];
const char *l_month[12];

struct country_code ctry[300];
struct response_code response[TOTAL_RC];


/*
  month name, short or long
  returns newly allocated string
*/
static char *
get_month_name(unsigned int month, bool short_name)
{
    char date[32];
    struct tm t;
    size_t name_size = 100;
    size_t ret;
    char *name = XMALLOC(char, name_size);

    snprintf(date, sizeof(date), "2001-%02u-12 18:31:01", month);
    strptime(date, "%Y-%m-%d %H:%M:%S", &t);
    ret = strftime(name, name_size, (short_name ? "%b" : "%B"), &t);
    if (ret == 0 || ret > name_size) {
        ERRVPRINT(VERBOSE0, "FATAL ERROR! get_month_name() failed: %d %d %d\n", month, short_name, ret);
        exit(1);
    }

    return name;
}


/***********************************************************************/

void
assign_messages(void)
{
    unsigned int i = 0;                         /* Basic counters for use with INIT_CTRY macro */
    unsigned int j = 0;
    unsigned month;

    h_msg = _("\
  -h, --help                 Print this help message\n\
  -d, --debug                Print additional debug info\n\
  -v, --verbose              Level of verbosity, Multiple v's will increase\n\
  -V, --version              Display the version information and exit\n\
\n\
  -c, --config=FILE          Use configuration file 'FILE'\n\
  -F, --logtype=TYPE         Log type. TYPE = (clf | ftp | squid |\n\
                               combined | domino | auto)\n\
  -m num                     Visit timout value (seconds)\n\
  -Z num                     Number of Months in Main Index\n\
\n\
  -f, --fold                 Fold sequence errors\n\
  -i, --ignore_history       Ignore history file\n\
  -p, --preserve_state       Preserve state (incremental)\n\
  -o, --output=DIR           Output directory to use\n\
  -T, --timing               Print timing information\n\
\n\
  -Y                         Suppress country graph\n\
  -G                         Suppress hourly graph\n\
  -H                         Suppress hourly stats\n\
  -L                         Suppress color coded graph legends\n\
  -l                         Suppress background lines on graph\n\
\n\
  -n name                    hostname to use\n\
  -t name                    report title 'name'\n\
  -a name                    hide user agent 'name'\n\
  -r name                    hide referrer 'name'\n\
  -s name                    hide site 'name'\n\
  -u name                    hide URL 'name'\n\
  -x name                    Use filename extension 'name'\n\
  -P name                    Page type extension 'name'\n\
  -I name                    Index alias 'name'\n\
\n\
  -A --top_agents=num        Display num top agents\n\
  -C --top_countries=num     Display num top countries\n\
  -R --top_refers=num        Display num top referrers\n\
  -S --top_sites=num         Display num top sites\n\
  -U --top_urls=num          Display num top URLs\n\
  -e --top_entry=num         Display num top Entry Pages\n\
  -E --top_exit=num          Display num top Exit Pages\n\
  -g num                     Group Domains to 'num' levels\n\
\n\
  --seg_country=COUNTRY      Segment based on the Country\n\
  --seg_referer=REFERER      Segment based on the Referal Text\n\
\n\
  -X                         Hide individual sites\n\
  -M num                     Mangle Agent Level (0-6; 0=disable)\n\
\n\
  --use_geoip                Enable GeoIP lookups\n\
  --match_counts             Display the various Group/Hide etc Match Counts\n\
  --disable_file_checks      Disable File Sytem Checks for Monthly Reports\n\
");


    for (month = 1; month <= 12; ++month) {
        s_month[month - 1] = get_month_name(month, true);
        l_month[month - 1] = get_month_name(month, false);
    }

/* Init the respone code array */
    memset(response, 0, sizeof(response));

    INIT_RESPCODE(IDX_UNDEFINED, _("Undefined response code"));
    INIT_RESPCODE(IDX_CONTINUE, _("Code 100 - Continue"));
    INIT_RESPCODE(IDX_SWITCHPROTO, _("Code 101 - Switching Protocols"));
    INIT_RESPCODE(IDX_OK, _("Code 200 - OK"));
    INIT_RESPCODE(IDX_CREATED, _("Code 201 - Created"));
    INIT_RESPCODE(IDX_ACCEPTED, _("Code 202 - Accepted"));
    INIT_RESPCODE(IDX_NONAUTHINFO, _("Code 203 - Non-Authoritative Information"));
    INIT_RESPCODE(IDX_NOCONTENT, _("Code 204 - No Content"));
    INIT_RESPCODE(IDX_RESETCONTENT, _("Code 205 - Reset Content"));
    INIT_RESPCODE(IDX_PARTIALCONTENT, _("Code 206 - Partial Content"));
    INIT_RESPCODE(IDX_MULTIPLECHOICES, _("Code 300 - Multiple Choices"));
    INIT_RESPCODE(IDX_MOVEDPERM, _("Code 301 - Moved Permanently"));
    INIT_RESPCODE(IDX_MOVEDTEMP, _("Code 302 - Found"));
    INIT_RESPCODE(IDX_SEEOTHER, _("Code 303 - See Other"));
    INIT_RESPCODE(IDX_NOMOD, _("Code 304 - Not Modified"));
    INIT_RESPCODE(IDX_USEPROXY, _("Code 305 - Use Proxy"));
    INIT_RESPCODE(IDX_MOVEDTEMPORARILY, _("Code 307 - Moved Temporarily"));
    INIT_RESPCODE(IDX_BAD, _("Code 400 - Bad Request"));
    INIT_RESPCODE(IDX_UNAUTH, _("Code 401 - Unauthorized"));
    INIT_RESPCODE(IDX_PAYMENTREQ, _("Code 402 - Payment Required"));
    INIT_RESPCODE(IDX_FORBIDDEN, _("Code 403 - Forbidden"));
    INIT_RESPCODE(IDX_NOTFOUND, _("Code 404 - Not Found"));
    INIT_RESPCODE(IDX_METHODNOTALLOWED, _("Code 405 - Method Not Allowed"));
    INIT_RESPCODE(IDX_NOTACCEPTABLE, _("Code 406 - Not Acceptable"));
    INIT_RESPCODE(IDX_PROXYAUTHREQ, _("Code 407 - Proxy Authentication Required"));
    INIT_RESPCODE(IDX_TIMEOUT, _("Code 408 - Request Timeout"));
    INIT_RESPCODE(IDX_CONFLICT, _("Code 409 - Conflict"));
    INIT_RESPCODE(IDX_GONE, _("Code 410 - Gone"));
    INIT_RESPCODE(IDX_LENGTHREQ, _("Code 411 - Length Required"));
    INIT_RESPCODE(IDX_PREFAILED, _("Code 412 - Precondition Failed"));
    INIT_RESPCODE(IDX_REQENTTOOLARGE, _("Code 413 - Request Entity Too Large"));
    INIT_RESPCODE(IDX_REQURITOOLARGE, _("Code 414 - Request-URI Too Long"));
    INIT_RESPCODE(IDX_UNSUPMEDIATYPE, _("Code 415 - Unsupported Media Type"));
    INIT_RESPCODE(IDX_RNGNOTSATISFIABLE, _("Code 416 - Requested Range Not Satisfiable"));
    INIT_RESPCODE(IDX_EXPECTATIONFAILED, _("Code 417 - Expectation Failed"));
    INIT_RESPCODE(IDX_SERVERERR, _("Code 500 - Internal Server Error"));
    INIT_RESPCODE(IDX_NOTIMPLEMENTED, _("Code 501 - Not Implemented"));
    INIT_RESPCODE(IDX_BADGATEWAY, _("Code 502 - Bad Gateway"));
    INIT_RESPCODE(IDX_UNAVAIL, _("Code 503 - Service Unavailable"));
    INIT_RESPCODE(IDX_GATEWAYTIMEOUT, _("Code 504 - Gateway Timeout"));
    INIT_RESPCODE(IDX_BADHTTPVER, _("Code 505 - HTTP Version Not Supported"));


/* Set up country domains. j, is used as a place holder within the INIT_CTRY macro only! */
    memset(ctry, 0, sizeof(ctry));

    INIT_CTRY(i++, "", _("Unresolved/Unknown"));

// Correct as at 21-Dec-2005 from http://www.iana.org/gtld/gtld.htm
    INIT_CTRY(i++, "aero", _("Air-Transport Industry"));
    INIT_CTRY(i++, "biz", _("Businesses"));
    INIT_CTRY(i++, "cat", _("Catalan Linguistic and Cultural Community"));
    INIT_CTRY(i++, "com", _("Commercial"));
    INIT_CTRY(i++, "coop", _("Cooperative Associations"));
    INIT_CTRY(i++, "info", _("Information"));
    INIT_CTRY(i++, "jobs", _("Human Resource Managers"));
    INIT_CTRY(i++, "mobi", _("Mobile Products and Services"));
    INIT_CTRY(i++, "museum", _("Museums"));
    INIT_CTRY(i++, "name", _("Individuals"));
    INIT_CTRY(i++, "net", _("Network"));
    INIT_CTRY(i++, "org", _("Non-Profit Organisation"));
    INIT_CTRY(i++, "pro", _("Credentialed Professionals"));
    INIT_CTRY(i++, "travel", _("Travel Industry"));
    INIT_CTRY(i++, "edu", _("US Educational"));
    INIT_CTRY(i++, "gov", _("US Government"));
    INIT_CTRY(i++, "mil", _("US Military"));
    INIT_CTRY(i++, "int", _("International Treaty Organisations"));
    INIT_CTRY(i++, "arpa", _("Old style Arpanet (arpa)"));
    INIT_CTRY(i++, "nato", _("NATO"));

// Correct as at 29-Apr-2005 from http://www.iana.org/cctld/cctld-whois.htm
    INIT_CTRY(i++, "ac", _("Ascension Island"));
    INIT_CTRY(i++, "ad", _("Andorra"));
    INIT_CTRY(i++, "ae", _("United Arab Emirates"));
    INIT_CTRY(i++, "af", _("Afghanistan"));
    INIT_CTRY(i++, "ag", _("Antigua and Barbuda"));
    INIT_CTRY(i++, "ai", _("Anguilla"));
    INIT_CTRY(i++, "al", _("Albania"));
    INIT_CTRY(i++, "am", _("Armenia"));
    INIT_CTRY(i++, "an", _("Netherlands Antilles"));
    INIT_CTRY(i++, "ao", _("Angola"));
    INIT_CTRY(i++, "aq", _("Antarctica"));
    INIT_CTRY(i++, "ar", _("Argentina"));
    INIT_CTRY(i++, "as", _("American Samoa"));
    INIT_CTRY(i++, "at", _("Austria"));
    INIT_CTRY(i++, "au", _("Australia"));
    INIT_CTRY(i++, "aw", _("Aruba"));
    INIT_CTRY(i++, "az", _("Azerbaijan"));
    INIT_CTRY(i++, "ax", _("Aland Islands"));
    INIT_CTRY(i++, "ba", _("Bosnia and Herzegovina"));
    INIT_CTRY(i++, "bb", _("Barbados"));
    INIT_CTRY(i++, "bd", _("Bangladesh"));
    INIT_CTRY(i++, "be", _("Belgium"));
    INIT_CTRY(i++, "bf", _("Burkina Faso"));
    INIT_CTRY(i++, "bg", _("Bulgaria"));
    INIT_CTRY(i++, "bh", _("Bahrain"));
    INIT_CTRY(i++, "bi", _("Burundi"));
    INIT_CTRY(i++, "bj", _("Benin"));
    INIT_CTRY(i++, "bm", _("Bermuda"));
    INIT_CTRY(i++, "bn", _("Brunei Darussalam"));
    INIT_CTRY(i++, "bo", _("Bolivia"));
    INIT_CTRY(i++, "br", _("Brazil"));
    INIT_CTRY(i++, "bs", _("Bahamas"));
    INIT_CTRY(i++, "bt", _("Bhutan"));
    INIT_CTRY(i++, "bv", _("Bouvet Island"));
    INIT_CTRY(i++, "bw", _("Botswana"));
    INIT_CTRY(i++, "by", _("Belarus"));
    INIT_CTRY(i++, "bz", _("Belize"));
    INIT_CTRY(i++, "ca", _("Canada"));
    INIT_CTRY(i++, "cc", _("Cocos (Keeling) Islands"));
    INIT_CTRY(i++, "cd", _("Congo, The Democratic Republic of the"));
    INIT_CTRY(i++, "cf", _("Central African Republic"));
    INIT_CTRY(i++, "cg", _("Congo, Republic of"));
    INIT_CTRY(i++, "ch", _("Switzerland"));
    INIT_CTRY(i++, "ci", _("Cote d'Ivoire"));
    INIT_CTRY(i++, "ck", _("Cook Islands"));
    INIT_CTRY(i++, "cl", _("Chile"));
    INIT_CTRY(i++, "cm", _("Cameroon"));
    INIT_CTRY(i++, "cn", _("China"));
    INIT_CTRY(i++, "co", _("Colombia"));
    INIT_CTRY(i++, "cr", _("Costa Rica"));
    INIT_CTRY(i++, "cs", _("Serbia and Montenegro"));
    INIT_CTRY(i++, "cu", _("Cuba"));
    INIT_CTRY(i++, "cv", _("Cape Verde"));
    INIT_CTRY(i++, "cx", _("Christmas Island"));
    INIT_CTRY(i++, "cy", _("Cyprus"));
    INIT_CTRY(i++, "cz", _("Czech Republic"));
    INIT_CTRY(i++, "de", _("Germany"));
    INIT_CTRY(i++, "dj", _("Djibouti"));
    INIT_CTRY(i++, "dk", _("Denmark"));
    INIT_CTRY(i++, "dm", _("Dominica"));
    INIT_CTRY(i++, "do", _("Dominican Republic"));
    INIT_CTRY(i++, "dz", _("Algeria"));
    INIT_CTRY(i++, "ec", _("Ecuador"));
    INIT_CTRY(i++, "ee", _("Estonia"));
    INIT_CTRY(i++, "eg", _("Egypt"));
    INIT_CTRY(i++, "eh", _("Western Sahara"));
    INIT_CTRY(i++, "er", _("Eritrea"));
    INIT_CTRY(i++, "es", _("Spain"));
    INIT_CTRY(i++, "et", _("Ethiopia"));
    INIT_CTRY(i++, "eu", _("European Union"));
    INIT_CTRY(i++, "fi", _("Finland"));
    INIT_CTRY(i++, "fj", _("Fiji"));
    INIT_CTRY(i++, "fk", _("Falkland Islands (Malvinas)"));
    INIT_CTRY(i++, "fm", _("Micronesia, Federal State of"));
    INIT_CTRY(i++, "fo", _("Faroe Islands"));
    INIT_CTRY(i++, "fr", _("France"));
    INIT_CTRY(i++, "ga", _("Gabon"));
    INIT_CTRY(i++, "gb", _("United Kingdom"));
    INIT_CTRY(i++, "gd", _("Grenada"));
    INIT_CTRY(i++, "ge", _("Georgia"));
    INIT_CTRY(i++, "gf", _("French Guiana"));
    INIT_CTRY(i++, "gg", _("Guernsey"));
    INIT_CTRY(i++, "gh", _("Ghana"));
    INIT_CTRY(i++, "gi", _("Gibraltar"));
    INIT_CTRY(i++, "gl", _("Greenland"));
    INIT_CTRY(i++, "gm", _("Gambia"));
    INIT_CTRY(i++, "gn", _("Guinea"));
    INIT_CTRY(i++, "gp", _("Guadeloupe"));
    INIT_CTRY(i++, "gq", _("Equatorial Guinea"));
    INIT_CTRY(i++, "gr", _("Greece"));
    INIT_CTRY(i++, "gs", _("South Georgia and the South Sandwich Islands"));
    INIT_CTRY(i++, "gt", _("Guatemala"));
    INIT_CTRY(i++, "gu", _("Guam"));
    INIT_CTRY(i++, "gw", _("Guinea-Bissau"));
    INIT_CTRY(i++, "gy", _("Guyana"));
    INIT_CTRY(i++, "hk", _("Hong Kong"));
    INIT_CTRY(i++, "hm", _("Heard and McDonald Islands"));
    INIT_CTRY(i++, "hn", _("Honduras"));
    INIT_CTRY(i++, "hr", _("Croatia/Hrvatska"));
    INIT_CTRY(i++, "ht", _("Haiti"));
    INIT_CTRY(i++, "hu", _("Hungary"));
    INIT_CTRY(i++, "id", _("Indonesia"));
    INIT_CTRY(i++, "ie", _("Ireland"));
    INIT_CTRY(i++, "il", _("Israel"));
    INIT_CTRY(i++, "im", _("Isle of Man"));
    INIT_CTRY(i++, "in", _("India"));
    INIT_CTRY(i++, "io", _("British Indian Ocean Territory"));
    INIT_CTRY(i++, "iq", _("Iraq"));
    INIT_CTRY(i++, "ir", _("Iran, Islamic Republic of"));
    INIT_CTRY(i++, "is", _("Iceland"));
    INIT_CTRY(i++, "it", _("Italy"));
    INIT_CTRY(i++, "je", _("Jersey"));
    INIT_CTRY(i++, "jm", _("Jamaica"));
    INIT_CTRY(i++, "jo", _("Jordan"));
    INIT_CTRY(i++, "jp", _("Japan"));
    INIT_CTRY(i++, "ke", _("Kenya"));
    INIT_CTRY(i++, "kg", _("Kyrgyzstan"));
    INIT_CTRY(i++, "kh", _("Cambodia"));
    INIT_CTRY(i++, "ki", _("Kiribati"));
    INIT_CTRY(i++, "km", _("Comoros"));
    INIT_CTRY(i++, "kn", _("Saint Kitts and Nevis"));
    INIT_CTRY(i++, "kp", _("Korea, Democratic People's Republic"));
    INIT_CTRY(i++, "kr", _("Korea, Republic of"));
    INIT_CTRY(i++, "kw", _("Kuwait"));
    INIT_CTRY(i++, "ky", _("Cayman Islands"));
    INIT_CTRY(i++, "kz", _("Kazakhstan"));
    INIT_CTRY(i++, "la", _("Lao People's Democratic Republic"));
    INIT_CTRY(i++, "lb", _("Lebanon"));
    INIT_CTRY(i++, "lc", _("Saint Lucia"));
    INIT_CTRY(i++, "li", _("Liechtenstein"));
    INIT_CTRY(i++, "lk", _("Sri Lanka"));
    INIT_CTRY(i++, "lr", _("Liberia"));
    INIT_CTRY(i++, "ls", _("Lesotho"));
    INIT_CTRY(i++, "lt", _("Lithuania"));
    INIT_CTRY(i++, "lu", _("Luxembourg"));
    INIT_CTRY(i++, "lv", _("Latvia"));
    INIT_CTRY(i++, "ly", _("Libyan Arab Jamahiriya"));
    INIT_CTRY(i++, "ma", _("Morocco"));
    INIT_CTRY(i++, "mc", _("Monaco"));
    INIT_CTRY(i++, "md", _("Moldova, Republic of"));
    INIT_CTRY(i++, "mg", _("Madagascar"));
    INIT_CTRY(i++, "mh", _("Marshall Islands"));
    INIT_CTRY(i++, "mk", _("Macedonia, The Former Yugoslav Republic of"));
    INIT_CTRY(i++, "ml", _("Mali"));
    INIT_CTRY(i++, "mm", _("Myanmar"));
    INIT_CTRY(i++, "mn", _("Mongolia"));
    INIT_CTRY(i++, "mo", _("Macau"));
    INIT_CTRY(i++, "mp", _("Northern Mariana Islands"));
    INIT_CTRY(i++, "mq", _("Martinique"));
    INIT_CTRY(i++, "mr", _("Mauritania"));
    INIT_CTRY(i++, "ms", _("Montserrat"));
    INIT_CTRY(i++, "mt", _("Malta"));
    INIT_CTRY(i++, "mu", _("Mauritius"));
    INIT_CTRY(i++, "mv", _("Maldives"));
    INIT_CTRY(i++, "mw", _("Malawi"));
    INIT_CTRY(i++, "mx", _("Mexico"));
    INIT_CTRY(i++, "my", _("Malaysia"));
    INIT_CTRY(i++, "mz", _("Mozambique"));
    INIT_CTRY(i++, "na", _("Namibia"));
    INIT_CTRY(i++, "nc", _("New Caledonia"));
    INIT_CTRY(i++, "ne", _("Niger"));
    INIT_CTRY(i++, "nf", _("Norfolk Island"));
    INIT_CTRY(i++, "ng", _("Nigeria"));
    INIT_CTRY(i++, "ni", _("Nicaragua"));
    INIT_CTRY(i++, "nl", _("Netherlands"));
    INIT_CTRY(i++, "no", _("Norway"));
    INIT_CTRY(i++, "np", _("Nepal"));
    INIT_CTRY(i++, "nr", _("Nauru"));
    INIT_CTRY(i++, "nu", _("Niue"));
    INIT_CTRY(i++, "nz", _("New Zealand"));
    INIT_CTRY(i++, "om", _("Oman"));
    INIT_CTRY(i++, "pa", _("Panama"));
    INIT_CTRY(i++, "pe", _("Peru"));
    INIT_CTRY(i++, "pf", _("French Polynesia"));
    INIT_CTRY(i++, "pg", _("Papua New Guinea"));
    INIT_CTRY(i++, "ph", _("Philippines"));
    INIT_CTRY(i++, "pk", _("Pakistan"));
    INIT_CTRY(i++, "pl", _("Poland"));
    INIT_CTRY(i++, "pm", _("Saint Pierre and Miquelon"));
    INIT_CTRY(i++, "pn", _("Pitcairn Island"));
    INIT_CTRY(i++, "pr", _("Puerto Rico"));
    INIT_CTRY(i++, "ps", _("Palestinian Territories"));
    INIT_CTRY(i++, "pt", _("Portugal"));
    INIT_CTRY(i++, "pw", _("Palau"));
    INIT_CTRY(i++, "py", _("Paraguay"));
    INIT_CTRY(i++, "qa", _("Qatar"));
    INIT_CTRY(i++, "re", _("Reunion Island"));
    INIT_CTRY(i++, "ro", _("Romania"));
    INIT_CTRY(i++, "ru", _("Russian Federation"));
    INIT_CTRY(i++, "rw", _("Rwanda"));
    INIT_CTRY(i++, "sa", _("Saudi Arabia"));
    INIT_CTRY(i++, "sb", _("Solomon Islands"));
    INIT_CTRY(i++, "sc", _("Seychelles"));
    INIT_CTRY(i++, "sd", _("Sudan"));
    INIT_CTRY(i++, "se", _("Sweden"));
    INIT_CTRY(i++, "sg", _("Singapore"));
    INIT_CTRY(i++, "sh", _("Saint Helena"));
    INIT_CTRY(i++, "si", _("Slovenia"));
    INIT_CTRY(i++, "sj", _("Svalbard and Jan Mayen Islands"));
    INIT_CTRY(i++, "sk", _("Slovak Republic"));
    INIT_CTRY(i++, "sl", _("Sierra Leone"));
    INIT_CTRY(i++, "sm", _("San Marino"));
    INIT_CTRY(i++, "sn", _("Senegal"));
    INIT_CTRY(i++, "so", _("Somalia"));
    INIT_CTRY(i++, "sr", _("Suriname"));
    INIT_CTRY(i++, "st", _("Sao Tome and Principe"));
    INIT_CTRY(i++, "sv", _("El Salvador"));
    INIT_CTRY(i++, "sy", _("Syrian Arab Republic"));
    INIT_CTRY(i++, "sz", _("Swaziland"));
    INIT_CTRY(i++, "tc", _("Turks and Caicos Islands"));
    INIT_CTRY(i++, "td", _("Chad"));
    INIT_CTRY(i++, "tf", _("French Southern Territories"));
    INIT_CTRY(i++, "tg", _("Togo"));
    INIT_CTRY(i++, "th", _("Thailand"));
    INIT_CTRY(i++, "tj", _("Tajikistan"));
    INIT_CTRY(i++, "tk", _("Tokelau"));
    INIT_CTRY(i++, "tl", _("Timor-Leste"));
    INIT_CTRY(i++, "tm", _("Turkmenistan"));
    INIT_CTRY(i++, "tn", _("Tunisia"));
    INIT_CTRY(i++, "to", _("Tonga"));
    INIT_CTRY(i++, "tp", _("East Timor"));
    INIT_CTRY(i++, "tr", _("Turkey"));
    INIT_CTRY(i++, "tt", _("Trinidad and Tobago"));
    INIT_CTRY(i++, "tv", _("Tuvalu"));
    INIT_CTRY(i++, "tw", _("Taiwan"));
    INIT_CTRY(i++, "tz", _("Tanzania"));
    INIT_CTRY(i++, "ua", _("Ukraine"));
    INIT_CTRY(i++, "ug", _("Uganda"));
    INIT_CTRY(i++, "uk", _("United Kingdom"));
    INIT_CTRY(i++, "um", _("United States Minor Outlying Islands"));
    INIT_CTRY(i++, "us", _("United States"));
    INIT_CTRY(i++, "uy", _("Uruguay"));
    INIT_CTRY(i++, "uz", _("Uzbekistan"));
    INIT_CTRY(i++, "va", _("Holy See (Vatican City State)"));
    INIT_CTRY(i++, "vc", _("Saint Vincent and the Grenadines"));
    INIT_CTRY(i++, "ve", _("Venezuela"));
    INIT_CTRY(i++, "vg", _("Virgin Islands, British"));
    INIT_CTRY(i++, "vi", _("Virgin Islands, U.S."));
    INIT_CTRY(i++, "vn", _("Vietnam"));
    INIT_CTRY(i++, "vu", _("Vanuatu"));
    INIT_CTRY(i++, "wf", _("Wallis and Futuna Islands"));
    INIT_CTRY(i++, "ws", _("Samoa"));
    INIT_CTRY(i++, "ye", _("Yemen"));
    INIT_CTRY(i++, "yt", _("Mayotte"));
    INIT_CTRY(i++, "yu", _("Yugoslavia"));
    INIT_CTRY(i++, "za", _("South Africa"));
    INIT_CTRY(i++, "zm", _("Zambia"));
    INIT_CTRY(i++, "zw", _("Zimbabwe"));

    ERRVPRINT(VERBOSE2, "Number of Country Codes Loaded: %u\n", i);

}
