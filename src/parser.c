/*
    AWFFull - A Webalizer Fork, Full o' features
    
    parser.c
        parsing log lines and individual records

    Copyright (C) 1997-2001  Bradford L. Barrett (brad@mrunix.net)
    Copyright (C) 2004-2008 by Stephen McInerney (spm@stedee.id.au)
    Copyright (C) 2006 by John Heaton (john@manchester.ac.uk)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

    This software uses the gd graphics library, which is copyright by
    Quest Protein Database Center, Cold Spring Harbor Labs.  Please
    see the documentation supplied with the library for additional
    information and license terms, or visit www.boutell.com/gd/ for the
    most recent version of the library and supporting documentation.

*/

#include "awffull.h"                            /* main header              */

/* internal function prototypes */
static int parse_record_web(char *, struct log_struct *);
static int parse_record_ftp(char *, struct log_struct *);
static int parse_record_squid(char *, struct log_struct *);

static int identify_log_format(char *);
static void re_compile_all_regexes(void);       /* Use at first run - compiles all used regex's */
static void re_compile_failed(int, const char *, char *);       /* Display a failed RE Compile & where */
static void re_check_errors(int);               /* After an RE check, deal with any errors */
static void error_substring_extract(int, int);  /* Error when we fail on getting a substring */

static pcre *cmp_log_regexp = NULL;             /* Main compiled RE - use as pointer only to one of the below */
static pcre *cmp_log_regexp_clf = NULL;         /* CLF compiled RE */
static pcre *cmp_log_regexp_combined = NULL;    /* Combined compiled RE */
static pcre *cmp_log_regexp_combined_enhanced = NULL;   /* Enhanced Combined compiled RE */
static pcre *cmp_log_regexp_xferlog = NULL;     /* FTP, xferlog format compiled RE */
static pcre *cmp_log_regexp_squid = NULL;       /* SQUID format compiled RE */
static pcre *cmp_log_regexp_domino = NULL;      /* Lotus Domino v6 format compiled RE */

//pcre_extra *studied_log_regexp = NULL;


/*********************************************/
/* PARSE_RECORD - uhhh, you know...          */
/*********************************************/

int
parse_record(char *buffer, struct log_struct *log_ptr)
//parse_record(char *buffer)
{
    int auto_log_type = 0;

    /* clear out structure */
//    memset(&log_rec, 0, sizeof(struct log_struct));

    if (cmp_log_regexp == NULL) {
        re_compile_all_regexes();
        if (g_settings.settings.log_type == LOG_AUTO) {
            auto_log_type = identify_log_format(buffer);
            if (auto_log_type > 0) {
                g_settings.settings.log_type = auto_log_type;
            } else {
                ERRVPRINT(VERBOSE0, "%s\n", _("Cannot recognise log format. Manually configure \"LogType\" in the config file."));
                exit(1);
            }
        }
        switch (g_settings.settings.log_type) {
        case LOG_FTP:
            cmp_log_regexp = cmp_log_regexp_xferlog;
            break;
        case LOG_SQUID:
            cmp_log_regexp = cmp_log_regexp_squid;
            break;
        case LOG_CLF:
            cmp_log_regexp = cmp_log_regexp_clf;
            break;
        case LOG_COMBINED:
            cmp_log_regexp = cmp_log_regexp_combined;
            break;
        case LOG_DOMINO:
            cmp_log_regexp = cmp_log_regexp_domino;
            break;
        default:
            ERRVPRINT(VERBOSE0, "%s %d\n", _("Unknown LOG Type Setting.:"), g_settings.settings.log_type);
            exit(1);
        }
    }

    /* call appropriate handler */
    switch (g_settings.settings.log_type) {
    default:
    case LOG_CLF:
    case LOG_COMBINED:
        return parse_record_web(buffer, log_ptr);
        break;                                  /* clf   */
    case LOG_FTP:
        return parse_record_ftp(buffer, log_ptr);
        break;                                  /* ftp   */
    case LOG_SQUID:
        return parse_record_squid(buffer, log_ptr);
        break;                                  /* squid */
    }
}

/*********************************************/
/* PARSE_RECORD_FTP - ftp log handler        */
/*********************************************/
static int
parse_record_ftp(char *buffer, struct log_struct *log_rec)
{
    int ovector[OVECCOUNT];                     /* RE substring offsets array */
    int rc;                                     /* RE Check return value */
    int copy_substr_rtn;                        /* RE Check return from pcre_copy_substring */

    int buffer_length;

    char tmp_bytes[25 + 1];
    char completion_status[2 + 1];

    buffer_length = (int) strlen(buffer);
    rc = pcre_exec(cmp_log_regexp, NULL, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);
    /* check for RE matching errors */
    if (rc < 0) {
        re_check_errors(rc);
        return (0);
    }

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 1, log_rec->datetime, 29);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 1);
        return (0);
    }

    /* Ignore time taken (in seconds) for now... */

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 3, log_rec->hostname, MAXHOST);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 3);
        return (0);
    }

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 4, tmp_bytes, 20);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 4);
        return (0);
    }
    log_rec->xfer_size = strtoul(tmp_bytes, NULL, 10);

    /* URL */
    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 5, log_rec->url, MAXURL);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 5);
        return (0);
    }

    /* Ignore Transfer Type */
    /* Ignore special-action-flag */
    /* Ignore Direction */

    /* User */
    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 10, log_rec->ident, MAXIDENT);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 10);
        return (0);
    }

    /* Completion Status - fake to a 200 or 206 */
    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 14, completion_status, 2);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 14);
        return (0);
    }
    if (completion_status[0] == 'i') {
        log_rec->resp_code = 206;
    } else {
        /* == c */
        log_rec->resp_code = 200;
    }

    return (1);
}

/*********************************************
 * PARSE_RECORD_WEB - web log handler        *
 * parse with pcre							 *
 *********************************************/
static int
parse_record_web(char *buffer, struct log_struct *log_rec)
{
    int ovector[OVECCOUNT];                     /* RE substring offsets array */
    int rc;                                     /* RE Check return value */
    int copy_substr_rtn;                        /* RE Check return from pcre_copy_substring */

    int buffer_length;

    char tmp_status[5 + 1];
    char tmp_bytes[20 + 1];

    buffer_length = (int) strlen(buffer);
//    rc = pcre_exec (cmp_log_regexp, studied_log_regexp, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);
    rc = pcre_exec(cmp_log_regexp, NULL, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);
    /* check for RE matching errors */
    if (rc < 0) {
        /* First see if a normal enhanced regex will work.
         * If this fails, then see if we can get a Domino style match
         *   If this works - switch to Domino Logs,
         *   If Fails - Boom.
         */
        if ((cmp_log_regexp != cmp_log_regexp_domino) && (cmp_log_regexp == cmp_log_regexp_combined) && (g_settings.settings.log_type == LOG_COMBINED)) {
            /* Attempt an enhanced log match */
            VPRINT(VERBOSE1, "%s\n", _("Attempting COMBINED_ENHANCED Regular Expression"));
            rc = pcre_exec(cmp_log_regexp_combined_enhanced, NULL, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);

            /* Didn't work. Try Domino? */
            if (rc < 0 && g_settings.flags.force_log_type == false) {
                /* Try a domino log format first - if is, switch to using domino checks instead */
                VPRINT(VERBOSE1, "%s\n", _("Attempting COMBINED_DOMINO Regular Expression"));
                rc = pcre_exec(cmp_log_regexp_domino, NULL, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);
                if (rc >= 0) {
                    /* Successfully matched as a Domino Log, apply domino RE from here on */
                    /* FIXME: The default domino RegEx is perhaps not as quick - is based on the ENHANCED */
                    VPRINT(VERBOSE1, "%s\n", _("Switching to DOMINO log format"));
                    cmp_log_regexp = cmp_log_regexp_domino;
                }
            }
        }
        if (rc < 0) {
            re_check_errors(rc);
            return (0);
        }
    }

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, LF_NCSA_ADDRESS, log_rec->hostname, MAXHOST - 1);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, LF_NCSA_ADDRESS);
        return (0);
    }

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, LF_NCSA_AUTHUSER, log_rec->ident, MAXIDENT - 1);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, LF_NCSA_USER);
        return (0);
    }

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, LF_NCSA_DATE_TIME, log_rec->datetime, MAXDATETIME - 1);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, LF_NCSA_DATE_TIME);
        return (0);
    }

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, LF_NCSA_URL, log_rec->url, MAXURL - 1);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, LF_NCSA_URL);
        return (0);
    }

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, LF_NCSA_STATUS, tmp_status, 5);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, LF_NCSA_STATUS);
        return (0);
    }
    log_rec->resp_code = atoi(tmp_status);

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, LF_NCSA_BYTES, tmp_bytes, 20);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, LF_NCSA_BYTES);
        return (0);
    }
    log_rec->xfer_size = strtoul(tmp_bytes, NULL, 10);

    if (g_settings.settings.log_type == LOG_COMBINED) {
        copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, LF_NCSA_REFERER, log_rec->refer, MAXREF - 1);
        if (copy_substr_rtn < 0) {
            error_substring_extract(copy_substr_rtn, LF_NCSA_REFERER);
            return (0);
        }
        copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, LF_NCSA_BROWSER, log_rec->agent, MAXAGENT - 1);
        if (copy_substr_rtn < 0) {
            error_substring_extract(copy_substr_rtn, LF_NCSA_BROWSER);
            return (0);
        }
    }
    return (1);
}


/*********************************************/
/* PARSE_RECORD_SQUID - squid log handler    */
/*********************************************/
static int
parse_record_squid(char *buffer, struct log_struct *log_rec)
{
    int ovector[OVECCOUNT];                     /* RE substring offsets array */
    int rc;                                     /* RE Check return value */
    int copy_substr_rtn;                        /* RE Check return from pcre_copy_substring */

    int buffer_length;

    char tmp_bytes[25 + 1];

    buffer_length = (int) strlen(buffer);
    rc = pcre_exec(cmp_log_regexp, NULL, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);
    /* check for RE matching errors */
    if (rc < 0) {
        re_check_errors(rc);
        return (0);
    }

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 1, log_rec->datetime, 29);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 1);
        return (0);
    }

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 4, log_rec->hostname, MAXHOST - 1);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 1);
        return (0);
    }

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 6, tmp_bytes, 20);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 1);
        return (0);
    }
    log_rec->resp_code = strtoul(tmp_bytes, NULL, 10);

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 7, tmp_bytes, 20);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 1);
        return (0);
    }
    log_rec->xfer_size = strtoul(tmp_bytes, NULL, 10);

    copy_substr_rtn = pcre_copy_substring(buffer, ovector, rc, 9, log_rec->url, MAXURL - 1);
    if (copy_substr_rtn < 0) {
        error_substring_extract(copy_substr_rtn, 1);
        return (0);
    }

    return (1);
}

/************************************************************************
 * parse_check_not_page                                                 *
 *                                                                      *
 * Given a URL (field from a log line)                                  *
 *   determine if this URL is a page or not                             *
 *                                                                      *
 * This function is the opposite of parse_is_page, and is only called   *
 * from same. It takes the linked list of "NotPageType" and applies     *
 * and RE against a log line built from that list.                      *
 *                                                                      *
 * A successfull match means that this line is NOT a page               *
 *                                                                      *
 * Arguments:                                                           *
 * -----------                                                          *
 * char *url      -  A URL field, typically: log_rec->url                *
 *                                                                      *
 * Returns:                                                             *
 * -----------                                                          *
 * Boolean.  True if, yes this is a page                                *
 *           False is not a page. ie Successfull match.                 *
 *                                                                      *
 ************************************************************************/
bool
parse_check_not_page(char *url)
{
    char regex_page[MAX_RE_LENGTH + 1] = "";    /* Hold the PAGE RE */
    static pcre *cmp_regex_page = NULL;         /* NotPage compiled RE */

    const char *error;                          /* RE error pointer, offset */
    int erroffset;                              /* RE error value */
    int str_length, tmp_length;
    int rc;                                     /* RE Check return value */
    static int max_type_length = 0;
    char *str_start = url;

    LISTPTR lptr = not_page_type;

    /* Compile both RegEx's */
    if (cmp_regex_page == NULL) {
        /* Build the RegEx first, loop thru the PageType list & reverse */
        strcat(regex_page, "\\.(");
        while (lptr != NULL) {
            strcat(regex_page, lptr->string);
            str_length = strlen(lptr->string);
            if (str_length > max_type_length) {
                max_type_length = str_length;
            }
            lptr = lptr->next;
            if (lptr != NULL) {
                strcat(regex_page, "|");
            }
        }
        strcat(regex_page, ")$");
        VPRINT(VERBOSE2, "PCRE: New NotPAGE RegEx: '%s',  Max: %d\n", regex_page, max_type_length);

        /* Compile the RegEx */
        cmp_regex_page = pcre_compile(regex_page, 0, &error, &erroffset, NULL);
        VPRINT(VERBOSE2, "PCRE: Compile PAGE%s", "\n");
        if (cmp_regex_page == NULL) {
            re_compile_failed(erroffset, error, regex_page);
        }
        max_type_length++;                      /* Increase by 1 for starting '.' */
    }

    str_length = strlen(url);
    if (str_length < max_type_length) {
        tmp_length = str_length;
    } else {
        tmp_length = max_type_length;
        str_start = url + str_length - max_type_length;
    }
    VPRINT(VERBOSE4, "  Was: '%s', Is: %s\n", url, str_start);

    rc = pcre_exec(cmp_regex_page, NULL, str_start, tmp_length, 0, 0, NULL, 0);
    /* check for RE matching */
    if (rc >= 0) {
        /* Have matched! */
        return (false);
    }
    return (true);
}

/************************************************************************
 * parse_is_page                                                        *
 *                                                                      *
 * Given a URL (field from a log line)                                  *
 *   determine if this URL is a page or not                             *
 *                                                                      *
 * Logic:                                                               *
 * -----------                                                          *
 * The RegEx and URL provided are reversed for efficiency - we only     *
 *  want to match the end of a URL, not the entire thing.               *
 * Firstly builds the RegEx. Does this by reversing the provided        *
 *  PageType config options, and wrappering appropriate RE around.
 *                                                                      *
 * Arguments:                                                           *
 * -----------                                                          *
 * char *url      -  A URL field, typically: log_rec->url                *
 *                                                                      *
 * Returns:                                                             *
 * -----------                                                          *
 * Boolean.  True if, yes this is a page                                *
 *           False in all other cases. Including invalid args.          *
 *                                                                      *
 ************************************************************************/

bool
parse_is_page(char *url)
{
    char regex_page[MAX_RE_LENGTH + 1] = "";    /* Hold the PAGE RE */
    static pcre *cmp_regex_page = NULL;         /* Page compiled RE */

    const char *error;                          /* RE error pointer, offset */
    int erroffset;                              /* RE error value */
    int str_length;
    int rc;                                     /* RE Check return value */
    char reverse[MAXURL + 1] = "";
    int i, j = 0;
    LISTPTR lptr;

    if (not_page_type != NULL) {
        return (parse_check_not_page(url));
    }

    lptr = page_type;

    /* Compile both RegEx's */
    if (cmp_regex_page == NULL) {
        /* Build the RegEx first, loop thru the PageType list & reverse */
        strcat(regex_page, "^(\\/|(");
        while (lptr != NULL) {
            str_length = (int) strlen(lptr->string);
            j = 0;
            for (i = str_length - 1; i >= 0; i--) {
                if (lptr->string[i] == '*') {
                    reverse[j] = '.';
                    j++;
                }
                reverse[j] = lptr->string[i];
                j++;
            }
            reverse[j] = '\0';
            strcat(regex_page, reverse);
            lptr = lptr->next;
            if (lptr != NULL) {
                strcat(regex_page, "|");
            }
        }
        strcat(regex_page, ")\\.|[^./]+\\/)");
        VPRINT(VERBOSE2, "PCRE: New PAGE RegEx: '%s'\n", regex_page);

        /* Compile the RegEx */
        cmp_regex_page = pcre_compile(regex_page, 0, &error, &erroffset, NULL);
        VPRINT(VERBOSE2, "PCRE: Compile PAGE%s", "\n");
        if (cmp_regex_page == NULL) {
            re_compile_failed(erroffset, error, regex_page);
        }
    }

    str_length = (int) strlen(url);
    j = 0;
    for (i = str_length - 1; i >= 0; i--) {
        /* Use pointer math - faster at loss of clarity */
        *(reverse + j) = *(url + i);
        j++;
    }
    *(reverse + j) = '\0';                      /* Probably not needed as we provide the length... */

    rc = pcre_exec(cmp_regex_page, NULL, reverse, str_length, 0, 0, NULL, 0);
    /* check for RE matching */
    if (rc >= 0) {
        /* Have matched! */
        return (true);
    }
    return (false);
}                                               /* parse_is_page */


/************************************************************************
 * identify_log_format                                                  *
 *                                                                      *
 * Attempt to identify the type of log format we've been given.         *
 * Returns the LOG_type as defined in awffull.h                         *
 * returns -1 if unknown.                                               *
 *                                                                      *
 * Requires a line of the log to attempt to process                     *
 ************************************************************************/
static int
identify_log_format(char *buffer)
{
    int ovector[OVECCOUNT];                     /* RE substring offsets array */
    int rc;                                     /* RE Check return value */
    int buffer_length;


    buffer_length = (int) strlen(buffer);

    /* Check for COMBINED */
    rc = pcre_exec(cmp_log_regexp_combined, NULL, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);
    if (rc >= 0) {
        /* Matches against COMBINED */
        VPRINT(VERBOSE1, "%s\n", _("Using COMBINED Log Format"));
        return (LOG_COMBINED);
    }

    /* Check for COMBINED_DOMINO */
    /* If the first line is a non logged in user, it'll probably register as COMBINED... */
    rc = pcre_exec(cmp_log_regexp_domino, NULL, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);
    if (rc >= 0) {
        /* Matches against COMBINED_DOMINO */
        VPRINT(VERBOSE1, "%s\n", _("Using COMBINED_DOMINO Log Format"));
        return (LOG_DOMINO);
    }

    rc = pcre_exec(cmp_log_regexp_clf, NULL, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);
    if (rc >= 0) {
        /* Matches against COMBINED */
        VPRINT(VERBOSE1, "%s\n", _("Using CLF Log Format"));
        return (LOG_CLF);
    }

    rc = pcre_exec(cmp_log_regexp_xferlog, NULL, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);
    if (rc >= 0) {
        /* Matches against FTP/XFERLOG */
        VPRINT(VERBOSE1, "%s\n", _("Using FTP/XFERLOG Log Format"));
        /* Invalid tables for this log type. Zero them away and hence not display. */
        g_settings.top.agents = 0;
        g_settings.top.refs = 0;
        return (LOG_FTP);
    }

    rc = pcre_exec(cmp_log_regexp_squid, NULL, buffer, buffer_length, 0, 0, ovector, OVECCOUNT);
    if (rc >= 0) {
        /* Matches against SQUID */
        VPRINT(VERBOSE1, "%s\n", _("Using SQUID Log Format"));
        /* Invalid tables for this log type. Zero them away and hence not display. */
        g_settings.top.agents = 0;
        g_settings.top.refs = 0;
        return (LOG_SQUID);
    }

    VPRINT(VERBOSE1, "%s\n", _("Unrecognised Log Format"));
    return (-1);                                /* Failed to match any, unknown format */
}


/************************************************************************
 * re_compile_all_regexs                                                *
 *                                                                      *
 * Does what the name says, in a single function we compile all         *
 *  possibly used Regular expressions.                                  *
 * Either forcibly exits on any failure, or happily finishes.           *
 * No values needed or returned.                                        *
 *                                                                      *
 * Assigns the RE's to the various globals:                             *
 *   cmp_log_regexp_*                                                   *
 ************************************************************************/
static void
re_compile_all_regexes(void)
{
    char log_regexp_clf[MAX_RE_LENGTH] = PATTERN_CLF;
    char log_regexp_combined[MAX_RE_LENGTH] = PATTERN_COMBINED;
    char log_regexp_combined_enhanced[MAX_RE_LENGTH] = PATTERN_COMBINED_ENHANCED;
    char log_regexp_xferlog[MAX_RE_LENGTH] = PATTERN_XFERLOG;
    char log_regexp_squid[MAX_RE_LENGTH] = PATTERN_SQUID;
    char log_regexp_domino[MAX_RE_LENGTH] = PATTERN_DOMINO;

    const char *error;                          /* RE error pointer, offset */
    int erroffset;                              /* RE error value */

    /* CLF */
    cmp_log_regexp_clf = pcre_compile(log_regexp_clf, 0, &error, &erroffset, NULL);
    VPRINT(VERBOSE2, "PCRE: Compile CLF%s", "\n");
    if (cmp_log_regexp_clf == NULL) {
        re_compile_failed(erroffset, error, log_regexp_clf);
    }

    /* Combined */
    cmp_log_regexp_combined = pcre_compile(log_regexp_combined, 0, &error, &erroffset, NULL);
    VPRINT(VERBOSE2, "PCRE: Compile COMBINED%s", "\n");
    if (cmp_log_regexp_combined == NULL) {
        re_compile_failed(erroffset, error, log_regexp_combined);
    }

    /* Enhanced Combined */
    cmp_log_regexp_combined_enhanced = pcre_compile(log_regexp_combined_enhanced, 0, &error, &erroffset, NULL);
    VPRINT(VERBOSE2, "PCRE: Compile COMBINED_ENHANCED%s", "\n");
    if (cmp_log_regexp_combined_enhanced == NULL) {
        re_compile_failed(erroffset, error, log_regexp_combined_enhanced);
    }

    /* FTP XFERLOG */
    cmp_log_regexp_xferlog = pcre_compile(log_regexp_xferlog, 0, &error, &erroffset, NULL);
    VPRINT(VERBOSE2, "PCRE: Compile PATTERN_XFERLOG%s", "\n");
    if (cmp_log_regexp_xferlog == NULL) {
        re_compile_failed(erroffset, error, log_regexp_xferlog);
    }

    /* SQUID LOG */
    cmp_log_regexp_squid = pcre_compile(log_regexp_squid, 0, &error, &erroffset, NULL);
    VPRINT(VERBOSE2, "PCRE: Compile PATTERN_SQUID%s", "\n");
    if (cmp_log_regexp_squid == NULL) {
        re_compile_failed(erroffset, error, log_regexp_squid);
    }

    /* DOMINO LOG */
    cmp_log_regexp_domino = pcre_compile(log_regexp_domino, 0, &error, &erroffset, NULL);
    VPRINT(VERBOSE2, "PCRE: Compile PATTERN_COMBINED_DOMINO%s", "\n");
    if (cmp_log_regexp_domino == NULL) {
        re_compile_failed(erroffset, error, log_regexp_domino);
    }
}


/************************************************************************
 * re_check_errors                                                      *
 *                                                                      *
 * After an RE check, deal with any errors                              *
 * err: value returned from pcre_exec                                   *
 * str_ptr: String that failed to match                                 *
 ************************************************************************/
static void
re_check_errors(int err)
{

    /* Matching failed: handle error cases */
    switch (err) {
    case PCRE_ERROR_NOMATCH:
        ERRVPRINT(VERBOSE1, "%s", _("Warning: No Regular Expression Match. "));
        break;
        /*  Leave out the more explicit failure messages - we show the number, so can be found.
           case PCRE_ERROR_NULL:
           case PCRE_ERROR_BADOPTION:
           case PCRE_ERROR_BADMAGIC:
           case PCRE_ERROR_UNKNOWN_NODE:
           case PCRE_ERROR_NOMEMORY:
           case PCRE_ERROR_NOSUBSTRING:
           case PCRE_ERROR_MATCHLIMIT:
           case PCRE_ERROR_CALLOUT:
           case PCRE_ERROR_BADUTF8:
           case PCRE_ERROR_BADUTF8_OFFSET:
           case PCRE_ERROR_PARTIAL:
           case PCRE_ERROR_BAD_PARTIAL:
           case PCRE_ERROR_INTERNAL:
           case PCRE_ERROR_BADCOUNT:
         */
    default:
        ERRVPRINT(VERBOSE1, "%s %d\n", _("Warning: Regular Expression Error:"), err);
        break;
    }
}


/************************************************************************
 * re_compile_failed                                                    *
 *                                                                      *
 * Display a failed RE Compile & where                                  *
 * FATAL failure. Will exit the run.                                    *
 ************************************************************************/
static void
re_compile_failed(int err, const char *err_offset, char *re_str)
{
    ERRVPRINT(VERBOSE0, "%s %d %s\n", _("FATAL ERROR! PCRE compilation failed at offset"), err, err_offset);
    ERRVPRINT(VERBOSE0, "%s %s\n", _("  Using Regular Expression:"), re_str);
    exit(1);                                    /* FIXME - table of exit codes! */
}


/************************************************************************
 * error_substring_extract                                              *
 *                                                                      *
 * Display a failed substring extraction                                *
 * Error Only, as this should have failed the RE                        *
 ************************************************************************/
static void
error_substring_extract(int err, int substr_idx)
{
    ERRVPRINT(VERBOSE1, "%s %d\n", _("Error: Failed to extract substring:"), substr_idx);
    switch (err) {
    case PCRE_ERROR_NOMEMORY:
        ERRVPRINT(VERBOSE2, "  PCRE: Insufficient Memory\n");
        break;
    case PCRE_ERROR_NOSUBSTRING:
        ERRVPRINT(VERBOSE2, "  PCRE: Substring doesn't exist.\n");
        break;
    default:
        ERRVPRINT(VERBOSE2, "  Unknown PCRE Error: %d\n", err);
        break;
    }
}


/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************/
