/*
    AWFFull - A Webalizer Fork, Full o' features

    xmalloc.c
        dealing with memory: functional wrappers

    Copyright (C) 2006, 2007, 2008 by Stephen McInerney
        (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "common.h"

/************************************************************************
 * xmalloc                                                              *
 *                                                                      *
 * Wrapper around calling calloc                                        *
 * Will also zero the allocated memory                                  *
 * **********************************************************************/
void *
xmalloc(size_t size)
{
    void *data_ptr;                             /* data pointer; result of calling calloc */

    data_ptr = calloc(1, size);
    if (data_ptr == NULL) {
        fprintf(stderr, "%s\n", "FATAL! Out Of Memory!");
        exit(1);
    }

    return (data_ptr);
}


/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************
 ************************************************************************/
