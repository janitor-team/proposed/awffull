/*
    AWFFull - A Webalizer Fork, Full o' features

    messages.h
        messages definitions/includes

    Copyright (C) 2004, 2005, 2007, 2008 by Stephen McInerney
        (spm@stedee.id.au)

    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef AWFFULL_LANG_H
#define AWFFULL_LANG_H

extern const char *h_msg;

extern const char *s_month[12];
extern const char *l_month[12];

extern struct response_code response[];
extern struct country_code ctry[];

#endif          /* AWFFULL_LANG_H */
