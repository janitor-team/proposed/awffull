/*
    AWFFull - A Webalizer Fork, Full o' features
    
    css_file.c
        Generate the CSS file

    Copyright (C) 2004-2008 by Stephen McInerney (spm@stedee.id.au)
    
    This file is part of AWFFull.

    AWFFull is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AWFFull is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AWFFull.  If not, see <http://www.gnu.org/licenses/>.
    
*/

#include "awffull.h"                            /* main header              */

const char *css[] = {
    "/* AWFFULL CSS File */",
    "p {font-size: small}       /* Default Paragraph */",
    "div {margin-top: 1em;}     /* Default div groups - simple separating */",

    "td {text-align: right; font-size: small; font-weight: bold;}   /* Default setting for ALL table data, everything else is the exception! */",

    "td.text {text-align: left; font-weight: normal; white-space: nowrap;}  /* Default text in tables */",
    "td.percent {font-size: x-small; font-weight: normal;}          /* Percent */",
    "td.special_percent {font-size: small; font-weight: normal;}      /* Special Percents */",

    "td.front_val {font-weight: normal;}    /* Values on the Main Index Page */",
    "td.front_str {font-weight: normal;}    /* Equiv to Val, used for Volume */",
    "td.front_tot_val {}                    /* Setting totals - same as default */",
    "td.front_tot_str {}                    /* ditto */",
    "td.front_month {font-weight: normal; text-align: left; white-space: nowrap;}   /* Link to the Month Page - intro text*/",
    "td.front_year {text-align: left; white-space: nowrap;} /* Subtotal for a Year - intro text */",
    "td.total {text-align: left; white-space: nowrap;}      /* Total for the entire table - intro text */",

    "th.front_monthhead {text-align: left; white-space: nowrap; background: " GREY ";}",
    "th.front_subhead {text-align: center; background: " GREY ";}",
    "th.index_key {text-align: center; background: " GREY ";}",

    "td.response_desc {text-align: left; font-weight: normal;}  /* Response Code Section */",
    "td.response_val {}",
    "td.response_pct {font-weight: normal;}",

    "td.summary_desc {text-align: left; font-weight: normal;}   /* Monthly Summary Section */",
    "td.summary_val {}",

    "td.index_count {text-align: center; font-weight: bold;}    /* Index Counts - day of month, hour etc */",

    "td.perday_desc {text-align: left; font-weight: normal;}    /* XXX per Day Section */",
    "td.perday_val {width: 65px;}",
    "td.perday_avg {width: 65px;}",

    "td.viewall {text-align: center; font-weight: normal;}      /* Trailing ViewAll XXX line */",
    "tr.viewall {background: " GRPCOLOR ";}",

    "tr.subtotal {background: " GREY ";}",
    "tr.total {background: " GREY ";}",
    "tr.group_shade {background: " GRPCOLOR ";}",

    "th {font-size: small; font-weight: bold; text-align: center;}    /* All the table headers */",
    "th.main {text-align: right; background: " GREY ";}",
    "th.hits {background: " DKGREEN ";}",
    "th.files {background: " BLUE ";}",
    "th.pages {background: " CYAN ";}",
    "th.visits {background: " YELLOW ";}",
    "th.sites {background: " ORANGE ";}",
    "th.transfer {background: " RED ";}",
    "th.bookmarks {background: " PURPLE ";}",
    "th.url {background: " ORANGE ";}",
    "th.agents {background: " ORANGE ";}",
    "th.search {background: " ORANGE ";}",
    "th.country {background: " CYAN ";}",
    "th.hostname {background: " ORANGE ";}",
    "th.referringurl {background: " LTBLUE ";}",
    "th.stickiness {background: " BROWN ";}",
    "th.popularity {background: " LTGREEN ";}",

    "img.flags {margin: 0; width: 25px; height: 15px; padding: 0 0.2em 0 0;}",

    "caption {text-align: center; font-weight: bold; background: " GREY "; border-width: 2;}    /* Table Caption */",

    "#navigation {font-size: small;}",
    "#monthTotal {font-size: small;}",
    "#response_code {text-align: center; font-size: small; font-weight: bold; background: " GREY ";}",
    "#summary_period {font-weight: bold;}",

    "#body {font-size: small; background: " LTGREY "; color: " BLACK ";}",
    "#header {text-align: left;}",
    "#footer {text-align: left;}",

    "h2 {font-size: x-large;}",

    "a:link {color: " BLUE ";}",
    "a:visited {color: " RED ";}",

    NULL
};

#define CSS_FILE_PERMISSIONS (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

/************************************************************************
 * generate_css_file
 *
 * Generates the CSS file to be used across all reports.
 * See above: const char *css[]
 *   for the default contents.
 * 
 * Additional Conditionals:
 * - Will always 'stat' for the css file at least once on every run
 * - Verbose1 will warn, once per run, to the CSS file existing already
 *
 ************************************************************************/
void
generate_css_file(void)
{
    unsigned int i = 0;
    int cssfile;
    ssize_t write_rtn;
    char lineout[MAXCSSLENGTH];
    static bool css_created = false;
    struct stat stat_buf;

    if (css_created != true) {
        if (stat(g_settings.settings.css_filename, &stat_buf) == -1) {
            cssfile = open(g_settings.settings.css_filename, O_WRONLY | O_CREAT | O_EXCL, CSS_FILE_PERMISSIONS);
            VPRINT(VERBOSE3, "CSS File Creation: %s\n", g_settings.settings.css_filename);
            if (cssfile >= 0) {
                while (css[i] != NULL) {
                    snprintf(lineout, sizeof(lineout), "%s\n", css[i]);
                    write_rtn = write(cssfile, lineout, strlen(lineout));
                    if (write_rtn == -1) {
                        ERRVPRINT(VERBOSE1, _("ERROR: Writing to CSS File \"%s\" failed.\n"), g_settings.settings.css_filename);
                        break;
                    }
                    i++;
                }
                close(cssfile);
            } else {
                ERRVPRINT(VERBOSE1, _("WARNING: Unable to create CSS File \"%s\".\n"), g_settings.settings.css_filename);
            }
        } else {
            ERRVPRINT(VERBOSE1, _("CSS File \"%s\" Exists. Not writing a new one.\n"), g_settings.settings.css_filename);
        }
        css_created = true;
    }
}

/************************************************************************
 ************************************************************************
 *                      END OF FILE                                     *
 ************************************************************************
 ************************************************************************/
